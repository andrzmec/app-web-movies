import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App.jsx'
import { Provider } from 'react-redux'
import generateStore  from './services/store/store'
import { IntlProvider } from 'react-intl'

const store = generateStore()

ReactDOM.render(
  <IntlProvider locale={'es'}>
    <Provider store={store}>
      <App />
    </Provider>
  </IntlProvider>,
  document.getElementById('root')
)
