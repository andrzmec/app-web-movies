import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import signinReducer, {readActiveUSerAction} from '../ducks/signin-duck'
import turnsReducer from '../ducks/turns-duck'
import moviesReducer from '../ducks/movies-duck'

const rootReducer = combineReducers({
    signin: signinReducer,
    turns: turnsReducer,
    movies: moviesReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk) ) )
    readActiveUSerAction()(store.dispatch)
    return store
}