// consts
const initData = {
    active: false,
    login: null
}


// types
const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS'
const SIGNIN_ERROR = 'SIGNIN_ERROR'
const USER_SIGNIN_SUCESS = 'USER_SIGNIN_SUCESS'
const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS'


// reducers
export default function signinReducer(state = initData, action){
    switch(action.type){
        case SIGNIN_SUCCESS:
            return {...state, active: action.payload}
        case SIGNIN_ERROR:
            return {...initData, login: false}
        case USER_SIGNIN_SUCESS:
            return {...state, active: true, user: action.payload}
        case SIGNOUT_SUCCESS:
            return {...initData}
        default:
            return state
    }
}


// actions
export const getSigninAction = (user, password ) =>  async(dispatch) => {
    if(user === 'admin' && password === 'Admin!123') {
        let res = {nickname: user, status: 'success'}
        dispatch({
            type: SIGNIN_SUCCESS,
            payload :  true
        })
        localStorage.setItem('app-web-user', JSON.stringify(res))
    } else {
        dispatch({
            type: SIGNIN_ERROR
        })
    }
}

export const readActiveUSerAction = () => async (dispatch) => {
    if(localStorage.getItem('app-web-user')) {
        dispatch({
            type: USER_SIGNIN_SUCESS,
            payload: JSON.parse(localStorage.getItem('app-web-user'))
        })
    }
}

export const logoutUserAction = () => (dispatch) => {
    localStorage.removeItem('app-web-user')
    dispatch({
        type: SIGNOUT_SUCCESS
    })
}

