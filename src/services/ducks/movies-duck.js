// consts
const initData = {
    results: [],
    select: null
}


// types
const MOVIES_SUCCESS = 'MOVIES_SUCCESS'
const MOVIES_ERROR = 'MOVIES_ERROR'
const MOVIES_UPDATE_SUCCESS = 'MOVIES_UPDATE_SUCCESS'
const MOVIES_UPDATE_ERROR = 'MOVIES_UPDATE_ERROR'
const MOVIES_SELECT_SUCCESS = 'MOVIES_SELECT_SUCCESS'
const MOVIES_SELECT_ERROR = 'MOVIES_SELECT_ERROR'


// reducers
export default function moviesReducer(state = initData, action){
    switch(action.type){
        case MOVIES_SUCCESS:
            return {...state, results: action.payload}
        case MOVIES_UPDATE_SUCCESS:
            return {...state, results: action.payload}
        case MOVIES_SELECT_SUCCESS:
            return {...state, select: action.payload}
        case MOVIES_UPDATE_ERROR:
            return {...state}
        case MOVIES_SELECT_ERROR:
            return {...state, select: null}
        case MOVIES_ERROR:
            return {...initData}
        default:
            return state
    }
}


// actions
export const getMoviesAction = () =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        dispatch({
            type: MOVIES_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
        return
    }
    try {
        let resp = []
        dispatch({
            type: MOVIES_SUCCESS,
            payload :  resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } catch (error) {
        dispatch({
            type: MOVIES_ERROR
        })
    }
}

export const addMoviesAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        resp.push(x)
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
        return
    }
    try {
        let resp = []
        resp.push(x)
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload :  resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } catch (error) {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}

export const removeMoviesAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        resp = resp.filter(item => item.id !== x)
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } else {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}

export const lockMoviesAction = (id, lock) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        let index = resp.findIndex(item => item.id === id)
        resp[index].lock = lock
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } else {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}

export const selectMoviesAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        resp = resp.filter(item => item.id === x)
        dispatch({
            type: MOVIES_SELECT_SUCCESS,
            payload: resp[0]
        })
    } else {
        dispatch({
            type: MOVIES_SELECT_ERROR
        })
    }
}

export const editMoviesAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        let index = resp.findIndex(item => item.id === x.id)
        resp[index].name = x.name
        resp[index].status = x.status
        resp[index].date = x.date
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } else {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}

export const turnsIntoMoviesAction = (id, turns) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        let index = resp.findIndex(item => item.id === id)
        resp[index].turns.push(turns)
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } else {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}

export const removeTurnsIntoMoviesAction = (id, turns) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-movies')) {
        let resp = JSON.parse(localStorage.getItem('app-web-movies'))
        let index = resp.findIndex(item => item.id === id)
        resp[index].turns = resp[index].turns.filter(item => item !== turns)
        dispatch({
            type: MOVIES_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-movies', JSON.stringify(resp))
    } else {
        dispatch({
            type: MOVIES_UPDATE_ERROR
        })
    }
}



