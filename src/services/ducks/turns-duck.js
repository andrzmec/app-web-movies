// consts
const initData = {
    results: [],
    select: null
}


// types
const TURNS_SUCCESS = 'TURNS_SUCCESS'
const TURNS_ERROR = 'TURNS_ERROR'
const TURNS_UPDATE_SUCCESS = 'TURNS_UPDATE_SUCCESS'
const TURNS_UPDATE_ERROR = 'TURNS_UPDATE_ERROR'
const TURNS_SELECT_SUCCESS = 'TURNS_SELECT_SUCCESS'
const TURNS_SELECT_ERROR = 'TURNS_SELECT_ERROR'


// reducers
export default function turnsReducer(state = initData, action){
    switch(action.type){
        case TURNS_SUCCESS:
            return {...state, results: action.payload}
        case TURNS_UPDATE_SUCCESS:
            return {...state, results: action.payload}
        case TURNS_SELECT_SUCCESS:
            return {...state, select: action.payload}
        case TURNS_UPDATE_ERROR:
            return {...state}
        case TURNS_SELECT_ERROR:
            return {...state, select: null}
        case TURNS_ERROR:
            return {...initData}
        default:
            return state
    }
}


// actions
export const getTurnsAction = () =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        dispatch({
            type: TURNS_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
        return
    }
    try {
        let resp = []
        dispatch({
            type: TURNS_SUCCESS,
            payload :  resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
    } catch (error) {
        dispatch({
            type: TURNS_ERROR
        })
    }
}

export const addTurnsAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        resp.push(x)
        dispatch({
            type: TURNS_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
        return
    }
    try {
        let resp = []
        resp.push(x)
        dispatch({
            type: TURNS_UPDATE_SUCCESS,
            payload :  resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
    } catch (error) {
        dispatch({
            type: TURNS_UPDATE_ERROR
        })
    }
}

export const removeTurnsAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        resp = resp.filter(item => item.id !== x)
        dispatch({
            type: TURNS_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
    } else {
        dispatch({
            type: TURNS_UPDATE_ERROR
        })
    }
}

export const lockTurnsAction = (id, lock) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        let index = resp.findIndex(item => item.id === id)
        resp[index].lock = lock
        dispatch({
            type: TURNS_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
    } else {
        dispatch({
            type: TURNS_UPDATE_ERROR
        })
    }
}

export const selectTurnsAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        resp = resp.filter(item => item.id === x)
        dispatch({
            type: TURNS_SELECT_SUCCESS,
            payload: resp[0]
        })
    } else {
        dispatch({
            type: TURNS_SELECT_ERROR
        })
    }
}

export const editTurnsAction = (x) =>  async(dispatch) => {
    if(localStorage.getItem('app-web-turns')) {
        let resp = JSON.parse(localStorage.getItem('app-web-turns'))
        let index = resp.findIndex(item => item.id === x.id)
        resp[index].turn = x.turn
        resp[index].status = x.status
        dispatch({
            type: TURNS_UPDATE_SUCCESS,
            payload: resp
        })
        localStorage.setItem('app-web-turns', JSON.stringify(resp))
    } else {
        dispatch({
            type: TURNS_UPDATE_ERROR
        })
    }
}



