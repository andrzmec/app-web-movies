import React, {useState, useEffect} from 'react'
import './ModalTurnsIntoMoviesComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { getTurnsAction } from '../../../services/ducks/turns-duck'
import { turnsIntoMoviesAction, selectMoviesAction, removeTurnsIntoMoviesAction } from '../../../services/ducks/movies-duck'

const ModalTurnsIntoMoviesComponent = (props) => {

    const dispatch = useDispatch()

    const selected = useSelector(store => store.movies.select)
    const turn = useSelector(store => store.turns.results)

    const [state, setstate] = useState(false)

    const [turns, setTurns] = useState('*')

    useEffect(() => {
        dispatch(getTurnsAction())
        let  timer = setTimeout(() => {
            setstate(true)
        }, 500)
        return () => clearTimeout(timer)
    },[selected.name, dispatch])


    const turnsChange = (e) => {
        setTurns(e.target.value)
        dispatch(turnsIntoMoviesAction(selected.id, e.target.value))
        dispatch(selectMoviesAction(selected.id))
    }

    const removeTurns = (id, turn) => {
        dispatch(removeTurnsIntoMoviesAction(id, turn))
        dispatch(selectMoviesAction(id))
    }

    return state ? (
        <Modal className="edit-turns-into-movies-modal" {...props} size="lg" aria-labelledby="ModalTurnsIntoMoviesComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="ModalTurnsIntoMoviesComponent">
                    Agregar turnos a {selected.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="container">
                    <select value={turns} onChange={turnsChange}>
                        <option value="*">Escoge turnos</option>
                        {
                            turn.length > 0 ? (
                                turn
                                .sort(function(a, b){
                                    if(a.turn < b.turn) { return -1 }
                                    if(a.turn > b.turn) { return 1 }
                                    return 0
                                })
                                .map((item, keyname) => (
                                    <option key={keyname}  value={item.turn} >{item.turn}</option>
                                ))
                            ): null
                        }
                    </select>
                    <div className="chips">
                        {
                            selected.turns
                            .sort(function(a, b){
                                if(a < b) { return -1 }
                                if(a > b) { return 1 }
                                return 0
                            })
                            .map((item, keyname) => (
                                <button className="chip" key={keyname} onClick={() => removeTurns(selected.id, item)}>{item} <i className="fas fa-times"></i></button>
                            ))
                        }
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    ): null
}

export default ModalTurnsIntoMoviesComponent
