import React from 'react'
import './MenuVerticalComponent.css'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import { Link, withRouter } from 'react-router-dom'

const MenuVerticalComponent = () => {
    return (
        <nav id="menu-vertical-movies">
            <div className="logo">
                <img src="https://dieteticallyspeaking.com/wp-content/uploads/2017/01/profile-square.jpg" alt="usuario admin"/>
            </div>
            <div className="navigation">
                <ul>
                    <li>
                        <OverlayTrigger 
                        placement="right" 
                        overlay={
                            <Tooltip>
                                Dashboard
                            </Tooltip>}>
                            <Link to="/main">
                                <i className="fas fa-photo-video"></i>
                            </Link>
                        </OverlayTrigger>
                    </li>
                    <li>
                        <OverlayTrigger 
                        placement="right" 
                        overlay={
                            <Tooltip>
                                Películas
                            </Tooltip>}>
                            <Link to="/movies">
                                <i className="fas fa-film"></i>
                            </Link>
                        </OverlayTrigger>
                    </li>
                    <li>
                        <OverlayTrigger 
                        placement="right" 
                        overlay={
                            <Tooltip>
                                Turnos
                            </Tooltip>}>
                            <Link to="/turns">
                                <i className="far fa-calendar-alt"></i>
                            </Link>
                        </OverlayTrigger>
                    </li>
                    <li>
                        <OverlayTrigger 
                        placement="right" 
                        overlay={
                            <Tooltip>
                                Administradores
                            </Tooltip>}>
                            <Link to="/admins">
                                <i className="fas fa-users"></i>
                            </Link>
                        </OverlayTrigger>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(MenuVerticalComponent)
