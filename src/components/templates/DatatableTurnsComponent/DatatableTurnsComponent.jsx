import React, { useState, useEffect } from 'react'
import './DatatableTurnsComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import DataTable from 'react-data-table-component'
import { getTurnsAction, lockTurnsAction, removeTurnsAction, selectTurnsAction } from '../../../services/ducks/turns-duck'
import ModalAddTurnsComponent from '../../templates/ModalAddTurnsComponent/ModalAddTurnsComponent'
import ModalEditTurnsComponent from '../../templates/ModalEditTurnsComponent/ModalEditTurnsComponent'

const DatatableTurnsComponent = () => {

  const dispatch = useDispatch()

  const turns = useSelector(store => store.turns.results)

  useEffect(() => {
    dispatch(getTurnsAction())
  }, [dispatch])

  const [modalAddTurn, setModalAddTurn] = useState(false)
  const [modalEditTurn, setModalEditTurn] = useState(false)

  const add = () => {
    setModalAddTurn(true)
  }

  const edit = (x) => {
    setModalEditTurn(true)
    dispatch(selectTurnsAction(x))
  }

  const lock = (x, y) => {
    dispatch(lockTurnsAction(x, y))
  }

  const remove = (x) => {
    dispatch(removeTurnsAction(x))
  }

  const lockIcon = (x) => {
    if(x) {
      return <i className="fas fa-lock"></i>
    } else {
      return <i className="fas fa-lock-open"></i>
    }
  }

  const buttons = (x, y) => {
    return (
      <div className="actions-datable">
        <button onClick={() => lock(x, !y)}>{lockIcon(y)}</button>
        <button onClick={() => edit(x)}><i className="fas fa-edit"></i></button>
        <button onClick={() => remove(x)}><i className="fas fa-trash-alt"></i></button>
      </div>
    )
  }

  const status = (x) => {
    if(x) {
      return <strong>ACTIVO</strong>
    } else {
      return <strong>INACTIVO</strong>
    }
  }

  const columns = [
      {
        name: 'Id',
        selector: 'id',
        sortable: true
      },
      {
        name: 'Turno',
        selector: 'turn',
        sortable: true,
        right: true
      },
      {
        name: 'Estado',
        selector: 'status',
        sortable: true,
        right: true,
        cell: row => status(row.status)
      },
      {
        name: '',
        right: true,
        cell: row => buttons(row.id, row.lock)
      }
  ]

  return (
      <>
          <div id="app-datatable-turns-movies">
              <div className="actions-turns">
                  <button onClick={() => add()}>Nuevo Turno</button>
              </div>
              <div className="table">
                <DataTable
                    title="Turnos"
                    columns={columns}
                    data={turns}
                    pagination={true}
                />
              </div>
          </div>
          {
            modalAddTurn && (<ModalAddTurnsComponent show={modalAddTurn} onHide={() => setModalAddTurn(false)}></ModalAddTurnsComponent>)
          }
          {
            modalEditTurn && (<ModalEditTurnsComponent show={modalEditTurn} onHide={() => setModalEditTurn(false)}></ModalEditTurnsComponent>)
          }
      </>
  )
}

export default DatatableTurnsComponent
