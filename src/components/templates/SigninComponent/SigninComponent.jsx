import React, {useState} from 'react'
import './SigninComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import { getSigninAction } from '../../../services/ducks/signin-duck'

const SiginComponent = () => {

    const dispatch = useDispatch()

    const auth = useSelector(store => store.signin.login)

    const [user, setUser] = useState('')
    const [password, setPassword] = useState('')

    const signinForm = e => {
        e.preventDefault()
        if(!user.trim() || !password.trim()){
            return
        } else {
            dispatch(getSigninAction(user, password))
        }
    }


    return (
        <div id="app-sigin-movies">
            <h3>Ingresa tu información para acceder a la plataforma...</h3>
            {
                auth === false && (
                    <span  variant={'danger'}>
                        Oops! compruebe su información
                    </span>
                )
            }
            <form onSubmit={signinForm}>
                <input type="text" value={user} placeholder="usuario" onChange={ e => setUser(e.target.value) } required/>
                <input type="password" value={password} placeholder="contraseña" onChange={ e => setPassword(e.target.value) } required/>
                <button type="submit">Acceder</button>
            </form>
        </div>
    )
}

export default SiginComponent
