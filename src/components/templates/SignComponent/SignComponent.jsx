/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from 'react'
import './SignComponent.css'
import SigninComponent from '../../templates/SigninComponent/SigninComponent'
import SignupComponent from '../../templates/SignupComponent/SignupComponent'

const SignComponent = () => {

    const [signup, setSignup] = useState(false)
    const [signin, setSignin] = useState(true)

    const goToSigIn = () => {
        setSignin(true)
        setSignup(false)
    }

    const goToSigUp = () => {
        setSignin(false)
        setSignup(true)
    }

    return (
        <div id="app-sign-movies">
            <div className="selection">
                <a onClick={() => goToSigIn()}>Iniciar Sesión</a> / <a onClick={() => goToSigUp()}>Registrarse</a>
            </div>
            {
                signin && (<SigninComponent></SigninComponent>)
            }
            {
                signup && (<SignupComponent></SignupComponent>)
            }
        </div>
    )
}

export default SignComponent
