import React from 'react'
import MenuHorizontalComponent from '../MenuHorizontalComponent/MenuHorizontalComponent'
import MenuVerticalComponent from '../MenuVerticalComponent/MenuVerticalComponent'

const MenuComponent = () => {
    return (
        <>
            <MenuHorizontalComponent></MenuHorizontalComponent>
            <MenuVerticalComponent></MenuVerticalComponent>
        </>
    )
}

export default MenuComponent
