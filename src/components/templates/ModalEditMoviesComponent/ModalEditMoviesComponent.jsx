import React, {useState, useEffect} from 'react'
import './ModalEditMoviesComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { editMoviesAction } from '../../../services/ducks/movies-duck'

const ModalEditMoviesComponent = (props) => {

    const dispatch = useDispatch()

    const selected = useSelector(store => store.movies.select)

    const [state, setstate] = useState(false)

    const [name, setname] = useState('')
    const [date, setdate] = useState('')
    const [active, setactive] = useState(true)

    useEffect(() => {
        let  timer = setTimeout(() => {
            setstate(true)
            setname(selected.name)
            setdate(selected.date)
            setactive(selected.status)
        }, 500)
        return () => clearTimeout(timer)
    },[selected.name, selected.date, selected.status])


    const updateForm = e => {
        e.preventDefault()
        let data = {
            id : selected.id,
            name: name,
            status: active,
            date: date
        }
        dispatch(editMoviesAction(data))
        props.onHide()
    }

    return state ? (
        <Modal className="edit-movies-modal" {...props} size="lg" aria-labelledby="ModalEditMoviesComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="ModalEditMoviesComponent">
                    Editar {selected.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    <form onSubmit={updateForm}>
                        <div className="input">
                            <span>Nombre de la Película</span>
                            <input type="text" value={name} placeholder="Ejemplo: Avengers..." onChange={ e => setname(e.target.value) } required/>
                        </div>
                        <div className="input">
                            <span>Fecha de publicación</span>
                            <input type="date" value={date} onChange={ e => setdate(e.target.value) } required/>
                        </div>
                        <div className="input">
                            <span>Activo</span>
                            <div className="switch_box box_1">
                                <input type="checkbox" className="switch_1" 
                                checked={active} 
                                onChange={() => setactive(!active)}/>
                            </div>
                        </div>
                        <button className="submit" type="submit">Actualizar Película</button>
                    </form>
                }
                
            </Modal.Body>
        </Modal>
    ): null
}

export default ModalEditMoviesComponent
