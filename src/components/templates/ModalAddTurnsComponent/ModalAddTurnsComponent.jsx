import React, {useState} from 'react'
import './ModalAddTurnsComponent.css'
import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import TimePicker from 'react-time-picker'
import { addTurnsAction } from '../../../services/ducks/turns-duck'
import shortid from 'shortid'

const ModalAddTurnsComponent = (props) => {

    const dispatch = useDispatch()

    const [turn, setturn] = useState('10:00')
    const [active, setactive] = useState(true)

    const onChange = time => setturn(time)

    const updateForm = e => {
        e.preventDefault()
        let data = {
            id: shortid.generate(),
            status: active,
            turn: turn,
            lock: false
        }
        dispatch(addTurnsAction(data))
        props.onHide()
    }

    return(
        <Modal className="add-turns-modal" {...props} size="lg" aria-labelledby="ModalAddTurnsComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="ModalAddTurnsComponent">
                    Nuevo Turno
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    <form onSubmit={updateForm}>
                        <div className="input">
                            <span>Turno</span>
                            <TimePicker
                            onChange={onChange}
                            value={turn}
                            format={'hh:mm'}
                            isOpen={true}
                            required={true}
                            autoFocus={true}
                            />
                        </div>
                        <div className="input">
                            <span>Activo</span>
                            <div className="switch_box box_1">
                                <input type="checkbox" className="switch_1" 
                                checked={active} 
                                onChange={() => setactive(!active)}/>
                            </div>
                        </div>
                        
                        <button className="submit" type="submit">Agregar Turno</button>
                    </form>
                }
                
            </Modal.Body>
        </Modal>
    )
}

export default ModalAddTurnsComponent
