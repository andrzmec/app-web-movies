/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import './MenuHorizontalComponent.css'
import { Dropdown } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { logoutUserAction } from '../../../services/ducks/signin-duck'
import { withRouter } from 'react-router-dom'

const MenuHorizontalComponent = (props) => {

    const dispatch = useDispatch()

    const logOut = () => {
        dispatch(logoutUserAction())
        props.history.push('/')
    }

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a href="#" ref={ref} onClick={(e) => { e.preventDefault(); onClick(e); }} >
          {children}
        </a>
    ))

    return (
        <nav id="menu-horizontal-movies">
            <div className="activities">
                <ul className="activities-items">
                    <li className="activities-item">
                        <Dropdown alignRight>
                            <Dropdown.Toggle as={CustomToggle}  id="ewisemaps-user-actions">
                                <i className="fas fa-bars"></i>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Header>Comparte el catálogo de películas</Dropdown.Header>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="facebook">
                                    <i className="fab fa-facebook-f"></i> <span>facebook</span>
                                </Dropdown.Item>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="twitter">
                                    <i className="fab fa-twitter"></i> <span>twitter</span>
                                </Dropdown.Item>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="linkedin">
                                    <i className="fab fa-linkedin-in"></i> <span>linkedin</span>
                                </Dropdown.Item>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="instagram">
                                    <i className="fab fa-instagram"></i> <span>instagram</span>
                                </Dropdown.Item>
                                <Dropdown.Divider />
                                <Dropdown.Header>¿Hay algún problema?</Dropdown.Header>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="bug">
                                    <i className="fas fa-bug"></i> <span>Reportar</span>
                                </Dropdown.Item>
                                <Dropdown.Header>USUARIO</Dropdown.Header>
                                <Dropdown.Item className="ewisemaps-user-item-actions" eventKey="logout" onClick={() => logOut()}>
                                    <i className="fas fa-sign-out-alt"></i> <span>Salir</span>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(MenuHorizontalComponent)
