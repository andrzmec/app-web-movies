import React, {useState} from 'react'
import './ModalAddMoviesComponent.css'
import { useDispatch } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { addMoviesAction } from '../../../services/ducks/movies-duck'
import shortid from 'shortid'

const ModalAddMoviesComponent = (props) => {

    const dispatch = useDispatch()

    const [name, setname] = useState('')
    const [date, setdate] = useState('')
    const [active, setactive] = useState(true)

    const updateForm = e => {
        e.preventDefault()
        let data = {
            id: shortid.generate(),
            name: name,
            date: date,
            lock: true,
            status: active,
            turns: []
        }
        dispatch(addMoviesAction(data))
        props.onHide()
    }

    return(
        <Modal className="add-movies-modal" {...props} size="lg" aria-labelledby="ModalAddMoviesComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="ModalAddMoviesComponent">
                    Nueva Película
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    <form onSubmit={updateForm}>
                        <div className="input">
                            <span>Nombre de la Película</span>
                            <input type="text" value={name} placeholder="Ejemplo: Avengers..." onChange={ e => setname(e.target.value) } required/>
                        </div>
                        <div className="input">
                            <span>Fecha de publicación</span>
                            <input type="date" value={date} onChange={ e => setdate(e.target.value) } required/>
                        </div>
                        <div className="input">
                            <span>Activo</span>
                            <div className="switch_box box_1">
                                <input type="checkbox" className="switch_1" 
                                checked={active} 
                                onChange={() => setactive(!active)}/>
                            </div>
                        </div>
                        <button className="submit" type="submit">Agregar Película</button>
                    </form>
                }
                
            </Modal.Body>
        </Modal>
    )
}

export default ModalAddMoviesComponent
