import React, { useState, useEffect } from 'react'
import './DatatableMoviesComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import DataTable from 'react-data-table-component'
import { getMoviesAction, lockMoviesAction, removeMoviesAction, selectMoviesAction } from '../../../services/ducks/movies-duck'
import ModalAddMoviesComponent from '../../templates/ModalAddMoviesComponent/ModalAddMoviesComponent'
import ModalEditMoviesComponent from '../../templates/ModalEditMoviesComponent/ModalEditMoviesComponent'
import ModalTurnsIntoMoviesComponent from '../ModalTurnsIntoMoviesComponent/ModalTurnsIntoMoviesComponent'

const DatatableMoviesComponent = () => {

  const dispatch = useDispatch()

  const movies = useSelector(store => store.movies.results)

  useEffect(() => {
    dispatch(getMoviesAction())
  }, [dispatch])

  const [modalAddMovies, setModalAddMovies] = useState(false)
  const [modalEditMovies, setModalEditMovies] = useState(false)
  const [modalTurnsIntoMovies, setModalTurnsIntoMovies] = useState(false)

  const add = () => {
    setModalAddMovies(true)
  }

  const edit = (x) => {
    setModalEditMovies(true)
    dispatch(selectMoviesAction(x))
  }

  const lock = (x, y) => {
    dispatch(lockMoviesAction(x, y))
  }

  const turnIntoMovie = (x) => {
    setModalTurnsIntoMovies(true)
    dispatch(selectMoviesAction(x))
  }

  const remove = (x) => {
    dispatch(removeMoviesAction(x))
  }

  const lockIcon = (x) => {
    if(x) {
      return <i className="fas fa-lock"></i>
    } else {
      return <i className="fas fa-lock-open"></i>
    }
  }

  const turnsIcon = (x) => {
    if(x.length > 0) {
      return <i className="far fa-calendar-alt"></i>
    } else {
      return <i className="far fa-calendar"></i>
    }
  }

  const buttons = (x, y, z) => {
    return (
      <div className="actions-datable">
        <button onClick={() => lock(x, !y)}>{lockIcon(y)}</button>
        <button onClick={() => turnIntoMovie(x)}>{turnsIcon(z)}</button>
        <button onClick={() => edit(x)}><i className="fas fa-edit"></i></button>
        <button onClick={() => remove(x)}><i className="fas fa-trash-alt"></i></button>
      </div>
    )
  }

  const status = (x) => {
    if(x) {
      return <strong>ACTIVO</strong>
    } else {
      return <strong>INACTIVO</strong>
    }
  }

  const columns = [
      {
        name: 'Id',
        selector: 'id',
        sortable: true
      },
      {
        name: 'Nombre',
        selector: 'name',
        sortable: true,
        right: true
      },
      {
        name: 'Fecha de publicación',
        selector: 'date',
        sortable: true,
        right: true
      },
      {
        name: 'Estado',
        selector: 'status',
        sortable: true,
        right: true,
        cell: row => status(row.status)
      },
      {
        name: '',
        right: true,
        cell: row => buttons(row.id, row.lock, row.turns)
      }
  ]

  return (
      <>
          <div id="app-datatable-movies-movies">
              <div className="actions-movies">
                  <button onClick={() => add()}>Nueva Película</button>
              </div>
              <div className="table">
                <DataTable
                    title="Películas"
                    columns={columns}
                    data={movies}
                    pagination={true}
                />
              </div>
          </div>
          {
            modalAddMovies && (<ModalAddMoviesComponent show={modalAddMovies} onHide={() => setModalAddMovies(false)}></ModalAddMoviesComponent>)
          }
          {
            modalEditMovies && (<ModalEditMoviesComponent show={modalEditMovies} onHide={() => setModalEditMovies(false)}></ModalEditMoviesComponent>)
          }
          {
            modalTurnsIntoMovies && (<ModalTurnsIntoMoviesComponent show={modalTurnsIntoMovies} onHide={() => setModalTurnsIntoMovies(false)}></ModalTurnsIntoMoviesComponent>)
          }
      </>
  )
}

export default DatatableMoviesComponent
