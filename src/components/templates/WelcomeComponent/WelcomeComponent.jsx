import React from 'react'
import './WelcomeComponent.css'
import 'fa-icons'

const WelcomeComponent = () => {
    return (
        <div id="app-welcome-movies">
            <div className="logo">
                <i className="fas fa-video"></i>
            </div>
            <div className="hello">
                <h1>Hola!!!</h1>
                <h4>Espero que te encuentres muy bien</h4>
            </div>
            <div className="info">
                <p>
                    Esta plataforma tiene como objetivo el manejo de información de las películas.
                </p>
            </div>
        </div>
    )
}

export default WelcomeComponent
