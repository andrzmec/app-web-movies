import React, {useState, useEffect} from 'react'
import './ModalEditTurnsComponent.css'
import { useDispatch, useSelector } from 'react-redux'
import { Modal } from 'react-bootstrap'
import TimePicker from 'react-time-picker'
import { editTurnsAction } from '../../../services/ducks/turns-duck'

const ModalEditTurnsComponent = (props) => {

    const dispatch = useDispatch()

    const selected = useSelector(store => store.turns.select)

    const [state, setstate] = useState(false)

    const [turn, setturn] = useState('10:00')
    const [active, setactive] = useState(true)

    useEffect(() => {
        let  timer = setTimeout(() => {
            setstate(true)
            setturn(selected.turn)
            setactive(selected.status)
        }, 500)
        return () => clearTimeout(timer)
    },[selected.turn,selected.status])


    const onChange = time => setturn(time)

    const updateForm = e => {
        e.preventDefault()
        let data = {
            id : selected.id,
            status: active,
            turn: turn
        }
        dispatch(editTurnsAction(data))
        props.onHide()
    }

    return state ? (
        <Modal className="edit-turns-modal" {...props} size="lg" aria-labelledby="ModalEditTurnsComponent" centered>
            <Modal.Header closeButton>
                <Modal.Title id="ModalEditTurnsComponent">
                    Editar Turno
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    <form onSubmit={updateForm}>
                        <div className="input">
                            <span>Turno</span>
                            <TimePicker
                            onChange={onChange}
                            value={turn}
                            format={'hh:mm'}
                            isOpen={true}
                            required={true}
                            autoFocus={true}
                            />
                        </div>
                        <div className="input">
                            <span>Activo</span>
                            <div className="switch_box box_1">
                                <input type="checkbox" className="switch_1" 
                                checked={active} 
                                onChange={() => setactive(!active)}/>
                            </div>
                        </div>
                        <button className="submit" type="submit">Actualizar Turno</button>
                    </form>
                }
                
            </Modal.Body>
        </Modal>
    ): null
}

export default ModalEditTurnsComponent
