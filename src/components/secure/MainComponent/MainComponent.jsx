import React, {useEffect} from 'react'
import './MainComponent.css'
import { useSelector } from 'react-redux'
import {withRouter} from 'react-router-dom'
import MenuComponent from '../../templates/MenuComponent/MenuComponent'

const MainComponent = (props) => {
    
    const active = useSelector(store => store.signin.active)

    useEffect(() => {
        if (active === false)  props.history.push('/')
    }, [active, props.history])

    return active ? (
        <>
            <MenuComponent></MenuComponent>
            <section id="app-main-movies">
            </section>
        </>
    ): null
}

export default withRouter(MainComponent)