import React, {useEffect} from 'react'
import './MoviesComponent.css'
import { useSelector } from 'react-redux'
import {withRouter} from 'react-router-dom'
import MenuComponent from '../../templates/MenuComponent/MenuComponent'
import DatatableMoviesComponent from '../../templates/DatatableMoviesComponent/DatatableMoviesComponent'

const MoviesComponent = (props) => {

    const active = useSelector(store => store.signin.active)

    useEffect(() => {
        if (active === false)  props.history.push('/')
    }, [active, props.history])


    return (
        <>
            <MenuComponent></MenuComponent>
            <section id="app-movies-movies">
                <DatatableMoviesComponent></DatatableMoviesComponent>
            </section>
        </>
    )
}

export default withRouter(MoviesComponent)
