import React, {useEffect} from 'react'
import './TurnsComponent.css'
import { useSelector } from 'react-redux'
import {withRouter} from 'react-router-dom'
import MenuComponent from '../../templates/MenuComponent/MenuComponent'
import DatatableTurnsComponent from '../../templates/DatatableTurnsComponent/DatatableTurnsComponent'

const TurnsComponent = (props) => {

    const active = useSelector(store => store.signin.active)

    useEffect(() => {
        if (active === false)  props.history.push('/')
    }, [active, props.history])


    return (
        <>
            <MenuComponent></MenuComponent>
            <section id="app-turns-movies">
                <DatatableTurnsComponent></DatatableTurnsComponent>
            </section>
        </>
    )
}

export default withRouter(TurnsComponent)
